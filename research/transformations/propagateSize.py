# encoding: utf-8

from __future__ import absolute_import, print_function, division

import theano.tensor as T

from ..model.base import ICalculationNode
from ..model.common import TFullyConnectedLayer
from .base import IPathTransformaion

__all__ = ['TPropagateSize']


class TPropagateSize(IPathTransformaion):

    @staticmethod
    def apply(path):
        transformed = [path[0]]

        def getNIn(to):
            for node in reversed(transformed):
                if isinstance(node, ICalculationNode):
                    return node.nOut

            raise RuntimeError('cannot deduce input dimension for {}'.format(to))

        for node in path[1:-1]:
            if isinstance(node, int):
                inDim = getNIn(node)
                node = TFullyConnectedLayer((inDim, node), T.tanh)
            else:
                node = node.copy()
            transformed.append(node)

        node = path[-1]
        if isinstance(node, int):
            inDim = getNIn(node)
            node = TFullyConnectedLayer((inDim, node), T.nnet.softmax)
        else:
            node = node.copy()
        transformed.append(node)
        return transformed
