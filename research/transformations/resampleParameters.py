# encoding: utf-8

from __future__ import absolute_import, print_function, division

from ..model.base import ICalculationNodeWithParams
from ..utils.misc import initRandomState
from .base import IPathTransformaion

__all__ = ['TResampleParameters']


class TResampleParameters(IPathTransformaion):
    _deconstruct_into_ = 'rng'

    def __init__(self, rng):
        self.rng = None if rng is None else initRandomState(rng)

    def apply(self, path):
        if self.rng is None:
            return path
        transformed = []
        for node in path:
            if isinstance(node, ICalculationNodeWithParams):
                node = node.resampleParameters(self.rng)
            transformed.append(node)

        return transformed
