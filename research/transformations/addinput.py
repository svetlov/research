# encoding: utf-8

from __future__ import absolute_import, print_function, division

from ..model.input import TInputLayer
from ..utils.misc import asInputs
from .base import IPathTransformaion

__all__ = ['TAddInputLayer']


class TAddInputLayer(IPathTransformaion):
    _deconstruct_into_ = 'inputs'

    def __init__(self, inputs):
        self.inputs = asInputs(inputs)

    def apply(self, path):
        value = path[0]
        if isinstance(value, TAddInputLayer):
            return path
        if not isinstance(value, int):
            raise RuntimeError('Cannot deduce shape of inputs.')
        inputLayer = TInputLayer(self.inputs, value)
        path = [inputLayer] + path[1:]
        return path
