# encoding: utf-8

from __future__ import absolute_import, print_function, division

import theano as th
import theano.tensor as T

from ..model.function import TFunctionLayer
from ..model.dropout import TDropout
from ..utils.misc import initRandomStream
from .base import ITrainTestPathTransformation

__all__ = ['TApplyDropout']


class TApplyDropout(ITrainTestPathTransformation):

    def __init__(self, srng=None):
        self.srng = initRandomStream(srng)

    def apply(self, trainPath, testPath):
        newTrainPath, newTestPath = [], []
        iterator = iter(zip(trainPath, testPath))
        for trainNode, testNode in iterator:
            if not isinstance(trainNode, TDropout):
                newTrainPath.append(trainNode)
                newTestPath.append(testNode)
                continue
            shape = newTrainPath[-1].shape
            nOut = shape[1]
            if isinstance(nOut, int):
                nOut = (nOut,)
            mask = self.srng.binomial(n=1, p=trainNode.retainProbability, size=nOut)
            mask = T.cast(mask, th.config.floatX)

            maskFunction = self._generateMaksFunction(mask)
            newTrainNode = TFunctionLayer(maskFunction, shape)
            newTrainPath.append(newTrainNode)
            newTestNode = TFunctionLayer(lambda x: x, shape)
            newTestPath.append(newTestNode)
            nextTrainNode, nextTestNode = next(iterator)
            newTrainPath.append(nextTrainNode)
            nextTestNode = nextTestNode.copy(W=nextTestNode.W * trainNode.retainProbability)
            newTestPath.append(nextTestNode)

        return (newTrainPath, newTestPath)

    @staticmethod
    def _generateMaksFunction(mask):
        def maskFunction(inputs):
            return mask * inputs

        return maskFunction
