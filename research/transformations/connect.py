# encoding: utf-8

from __future__ import absolute_import, print_function, division

from .base import ITrainTestPathTransformation

__all__ = ['TConnectLayers']


class TConnectLayers(ITrainTestPathTransformation):

    @staticmethod
    def apply(trainPath, testPath):
        connectedTrainPath = TConnectLayers.connect(trainPath)
        connectedTestPath = TConnectLayers.connect(testPath)
        return (connectedTrainPath, connectedTestPath)

    @staticmethod
    def connect(sequence):
        connected = [sequence[0]]
        inputs = sequence[0].outputs
        for node in sequence[1:]:
            connectedNode = node.copy(inputs=inputs)
            connected.append(connectedNode)
            inputs = connectedNode.outputs

        return connected
