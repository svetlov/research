# encoding: utf-8

from __future__ import absolute_import, print_function, division

from ..utils.deconstruct import IDeconstructible

__all__ = ['IPathTransformaion', 'ITrainTestPathTransformation']


class ITransformationBase(IDeconstructible):

    def apply(self, *args, **kwargs):
        raise NotImplementedError


class IPathTransformaion(ITransformationBase):

    def apply(self, path):
        raise NotImplementedError


class ITrainTestPathTransformation(ITransformationBase):

    def apply(self, trainPath, testPath):
        raise NotImplementedError
