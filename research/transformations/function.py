# encoding: utf-8

from __future__ import absolute_import, print_function, division

import inspect

from ..model.function import TFunctionLayer
from .base import ITransformationBase

__all__ = ['TExpandFunction']


class TExpandFunction(ITransformationBase):
    @staticmethod
    def apply(path):
        transformed = []
        for node in path:
            if not inspect.isfunction(node):
                transformed.append(node)
                continue
            nOut = transformed[-1].shape[1]
            shape = (nOut, nOut)
            transformed.append(TFunctionLayer(node, shape))
        return transformed
