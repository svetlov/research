# encoding: utf-8

from __future__ import absolute_import, division, print_function

from itertools import groupby, izip
from collections import defaultdict
from operator import itemgetter

import numpy as np


class UniteNodeBuilder(object):
    def __init__(self, trnData, vldData, buildModelAndTrainer, getWinner):
        model, trainer = buildModelAndTrainer(trnData, vldData)

        self.trainData = trnData
        self.validationData = vldData
        self.model = model
        self.trainer = trainer
        self.getWinner = getWinner

        self.mappings = {0: self.trainData.labelToTarget.copy()}

    def tryUniteOutputs(self):
        outputs = self.model.testPath.apply(self.trainData.features)

        votes = np.zeros(
            [self.trainData.nLabels, outputs.shape[1]],
            dtype=int
        )

        winners = self.getWinner(outputs)
        for index in xrange(self.trainData.size):
            if winners[index] == -1:
                continue
            clsNo = self.trainData.labels[index]
            realOutput = winners[index]
            votes[clsNo][realOutput] += 1

        maxVotesClasses = votes.argmax(axis=1)
        maxVotes = votes[np.arange(self.trainData.nLabels), maxVotesClasses]

        transferMatrix = self.trainData.labelToTarget.copy()
        # calc size of each label
        labelSizes = {}
        for label in transferMatrix.keys():
            count = np.where(self.trainData.labels == label)[0].size
            labelSizes[label] = count

        for clsNo in xrange(self.trainData.nLabels):
            if maxVotes[clsNo] > labelSizes[clsNo] * 0.5:
                if transferMatrix[clsNo] != maxVotesClasses[clsNo]:
                    print("trying to remap label {} clsNo to {}".format(
                        clsNo, maxVotesClasses[clsNo]
                    ))
                transferMatrix[clsNo] = maxVotesClasses[clsNo]

        activeNeurons = {neuron for neuron in transferMatrix.values()}
        if len(activeNeurons) == 1:
            return

        self.trainData.labelToTarget = transferMatrix
        self.validationData.labelToTarget = self.trainData.labelToTarget

    def build(self, starting, timeout):
        batchIterations = self.trainer.train()
        epochs = groupby(batchIterations, itemgetter(0))
        for epoch, iterations in epochs:
            for iteration in iterations:
                last = iteration
            if epoch >= starting and (epoch % timeout) == 0:
                self.mappings[epoch] = self.trainData.labelToTarget.copy()
                self.tryUniteOutputs()
        print("After training mapping is:")
        for label, target in self.mappings[epoch].items():
            print("map label {} -> neuron {}".format(label, target))

        return self.model


class UniteClusterizationBuilder(object):
    def __init__(self, trnData, buildModelAndTrainer, getWinner):
        model, trainer = buildModelAndTrainer(trnData)

        self.trnData, self.model, self.trainer = trnData, model, trainer
        self.getWinner = getWinner

    def build(self, startUnite, timeout, endUnite, nEpochs):
        for iEpoch in xrange(nEpochs):
            print("Train epoch number {}".format(iEpoch))
            self.trainer.trainEpoch()
            if (startUnite <= iEpoch <= endUnite and
                    (iEpoch - startUnite) % timeout == 0):
                self.tryUniteOutputs()

    def tryUniteOutputs(self):
        outputs = self.model.testPath.apply(self.trnData.features)
        winners = self.getWinners(outputs)
        targets = self.trnData.originalTargets
        assert winners.shape == targets.shape

        # current target->neuronIdx mapping
        currentMapping = self.trnData.targetToNeuronMapping
        newMapping = currentMapping.copy()

        targetSizes = defaultdict(int)
        votes = defaultdict(lambda: defaultdict(int))
        for target, winner in izip(targets, winners):
            targetSizes[target] += 1
            votes[target][winner] += 1

        for target, targetVotes in votes.iteritems():
            maxVotes, maxVotesNeuronIdx = 0, None
            for neuronIdx, votesInThisNeuronIdx in targetVotes.iteritems():
                if maxVotes < votesInThisNeuronIdx:
                    maxVotes = votesInThisNeuronIdx
                    maxVotesNeuronIdx = neuronIdx
            if (maxVotesNeuronIdx is None or
                    maxVotes < targetSizes[target] * 0.5):
                newMapping[target] = currentMapping[target]
            else:
                newMapping[target] = maxVotesNeuronIdx
        self.trnData.targetToNeuronMapping = newMapping
