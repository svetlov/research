# encoding: utf-8


class IBatchTrainer(object):
    def __init__(self, data):
        self._data = data

    def trainEpoch(self):
        raise NotImplementedError

    def trainBatch(self, batchNo):
        raise NotImplementedError

    @property
    def batchSize(self):
        return self._data.batchSize

    @property
    def nBatches(self):
        return self._data.nBatches
