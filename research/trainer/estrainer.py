# encoding: utf-8

from __future__ import print_function

import numpy as np

from ..utils.loss import makeLossEstimator


class TEarlyStoppingGradientTrainer(object):
    def __init__(self, trainer, validate):
        self.trainer = trainer
        self.validate = validate

        self.errorScore = self.bestError = np.inf
        self.bestParams = [param.get_value() for param in trainer.model.params if hasattr(param, 'get_value')]
        self.bestError = validate()

    def _checkIsNewBestError(self):
        self.errorScore = self.validate()
        if self.errorScore < self.bestError:
            self.bestError = self.errorScore
            self.bestParams = [param.get_value() for param in self.trainer.model.params if hasattr(param, 'get_value')]
            return True

    def train(self, nEpochs=2000, patience=5000, patienceIncrease=2,
              improvementThreshold=0.998, validationFrequency=None):
        if validationFrequency is None:
            validationFrequency = min(self.trainer.nBatches, patience / 2)

        self._checkIsNewBestError()
        signVLDLossChanges = self.bestError * improvementThreshold

        epoch, totalBatchIndex, = 0, 0
        while epoch < nEpochs and totalBatchIndex <= patience:
            epoch += 1
            for batchIndex in xrange(self.trainer.nBatches):
                totalBatchIndex += 1
                self.trainer.trainBatch()
                if (totalBatchIndex + 1) % validationFrequency == 0:
                    if self._checkIsNewBestError():
                        if self.bestError < signVLDLossChanges:
                            patience = max(
                                patience, totalBatchIndex * patienceIncrease
                            )
                        signVLDLossChanges = self.bestError * improvementThreshold
                yield epoch, batchIndex, self.errorScore

        params = zip([param for param in self.trainer.model.params if hasattr(param, 'set_value')], self.bestParams)
        for modelParam, bestParam in params:
            modelParam.set_value(bestParam, borrow=True)
