# encoding: utf-8

from __future__ import print_function

import numpy as np
import theano
import theano.tensor as T

from ..model.base import ITrainTestCalculationPath
from .base import IBatchTrainer
from ..utils.loss import makeLossEstimator


class TRpropTrainer(IBatchTrainer):
    def __init__(
            self, model, data, loss,
            plus=1.2, minus=0.5,
            maximum=50.,
            minimum = 0.000001
    ):
        if isinstance(model, ITrainTestCalculationPath):
            model = model.trainPath
        self.model = model
        self.data = data
        self.loss = loss

        assert self.data.nBatches == 1

        self.plus = plus
        self.minus = minus
        self.maximumDelta = maximum
        self.minimumDelta = minimum

        self.train, self._updates = self._initTrainFunction()

    def _initTrainFunction(self):
        loss = self.loss(self.model.outputs, self.data.targetsVariable)
        updates = []
        for param in self.model.params:
            prevGradSgn = theano.shared(param.get_value() * 0.+ 1.0)
            prevDelta = theano.shared(param.get_value() * 0. + 0.1)
            prevWeigthsUpdate = theano.shared(param.get_value() * 0. + 1)

            gradSgn = T.sgn(T.grad(loss, param))
            mulGradSgn = T.mul(gradSgn, prevGradSgn)
            mulGradGt = T.gt(mulGradSgn, 0.0)
            mulGradLt = T.lt(mulGradSgn, 0.0)
            mulGradEq = T.eq(mulGradSgn, 0.0)
            mulGradGeq = mulGradGt + mulGradEq

            deltaPlus = T.minimum(
                self.plus * T.mul(prevDelta, mulGradGt),
                self.maximumDelta * mulGradGt
            )
            wPlus = - 1. * T.mul(gradSgn, deltaPlus)
            wPlusUpdates = wPlus

            deltaMinus = T.maximum(
                self.minus * T.mul(prevDelta, mulGradLt),
                self.minimumDelta * mulGradLt
            )
            wMinus = T.mul(prevWeigthsUpdate, mulGradLt)  # deltaMinus
            wMinusUpdates = -1. * T.mul(prevWeigthsUpdate, mulGradLt)
            correctedGradient = T.mul(mulGradGeq, gradSgn)

            deltaEqual = T.mul(prevDelta, mulGradEq)
            wEqual = -1. * T.mul(gradSgn, deltaEqual)
            wEqualUpdates = wEqual

            updates.append((
                param, param + wPlusUpdates + wMinusUpdates + wEqualUpdates
            ))
            updates.append((prevWeigthsUpdate, wPlus + wMinus + wEqual))
            updates.append((prevDelta, deltaPlus + deltaMinus + deltaEqual))
            updates.append((prevGradSgn, correctedGradient))

        train = makeLossEstimator(self.model, self.data, loss, updates)
        return train, updates
