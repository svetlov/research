# encoding: utf-8

from __future__ import print_function

from collections import defaultdict

import sys
import numpy as np
import theano as th
from theano import tensor as T

from ..model import ITrainTestCalculationPath
from ..utils.loss import makeLossEstimator
from .base import IBatchTrainer


class TMomentumTrainer(IBatchTrainer):
    def __init__(self, model, data, loss, alpha, momentum, logger=None):
        if isinstance(model, ITrainTestCalculationPath):
            self.model = model.trainPath
        else:
            self.model = model
        self._data = data
        self.loss = loss
        if not isinstance(alpha, (tuple, list)):
            alpha = [alpha for _ in self.model.params]
        self.learningRates = alpha
        if not isinstance(momentum, (tuple, list)):
            momentum = [momentum for _ in self.model.params]
        self.momentums = momentum
        self.trainBatch, self._updates = self._initTrainFunction()

    def _initTrainFunction(self):
        loss = self.loss(self.model.outputs, self._data.expectedTargetsVariable)
        updates = list(self._data.updates)
        params = defaultdict(list)  # param: [(lr1, momentum1), (lr2, momentum2), ...]; list is for shared params
        paramsByLayers = zip(self.learningRates, self.momentums, self.model.params)
        for alpha, momentum, param in paramsByLayers:
            params[param].append([alpha, momentum])

        for param, hyperparams in params.items():
            gradient = T.grad(loss, param)
            param_update = th.shared(
                param.get_value() * 0.,
                broadcastable=param.broadcastable,
                # TODO (splinter 2015/12/27) fix dtype
            )
            deltas = [T.cast(alpha * param_update, th.config.floatX) for alpha, momentum in hyperparams]
            deltas = reduce(lambda x, y: x - y, deltas, param)
            updates.append((param, deltas))
            updatedeltas = sum([(momentum * param_update + (1.0 - momentum) * gradient) for alpha, momentum in hyperparams])
            updates.append((param_update, updatedeltas))

        train = makeLossEstimator(self.model, self._data, loss, updates)
        return (train, updates)

    def trainEpoch(self):
        return np.mean([
            self.trainBatch() for _ in xrange(self.nBatches)
        ])
