# encoding: utf-8

import sys
import numpy
import pandas


def normalize(data, without=None):
    shifts, factors = {}, {}
    without = [] if without is None else (without if isinstance(without, (list, tuple)) else [without])
    interested = set([name for name in data.columns if name not in without])
    for name in interested:
        maximum, minimum, mean = data[name].max(), data[name].min(), data[name].mean()
        shifts[name] = mean
        factors[name] = 1.0 / (maximum - minimum)

    def transform(data):
        assert interested.issubset(set(data.columns))
        withoutTransformation = set(data.columns) - interested
        transformedData = {name: data[name] for name in withoutTransformation}
        for name in interested:
            transformedData[name] = (data[name] - shifts[name]) * 2 * factors[name]
        return pandas.DataFrame(transformedData)

    return transform


def shuffle(data):
    order = numpy.random.permutation(data.index)
    return data[order]


def splitData(data, proportions, groupby):
    assert sum(proportions) - 1.0 < sys.float_info.epsilon
    nParts = len(proportions)
    parts = [list() for _ in xrange(nParts)]

    intervals = []
    for ratio in proportions:
        begin = intervals[-1][1] if len(intervals) > 0 else 0.0
        end = begin + ratio
        intervals.append([begin, end])
    for key, records in data.groupby(groupby):
        shuffled = numpy.random.permutation(records.index)
        for (begin, end), part in zip(intervals, parts):
            startIndex = int(begin * len(shuffled))
            endIndex = int(end * len(shuffled))
            part.extend(shuffled[startIndex:endIndex])
    parts = [numpy.random.permutation(rows) for rows in parts]
    return [data.iloc[rows] for rows in parts]


def separateLabels(data, labelsColumns, flatten=True):
    labelsColumns = [labelsColumns, ] if not isinstance(labelsColumns, (list, tuple)) else labelsColumns
    featuresColumns = list(set(data.columns) - set(labelsColumns))
    features, labels = data[featuresColumns], data[labelsColumns]
    labels = labels.values.flatten() if flatten else labels.values
    return features.values, labels
