# encoding: utf-8

from __future__ import absolute_import, print_function, division

import numpy as np
import theano as th

from ..utils.deconstruct import IDeconstructible

__all__ = [
    'ICalculationNode',
    'ICalculationNodeWithParams',
    'ICalculationPath',
    'ICalculationPathWithParams',
    'ITrainTestCalculationPath'
]


class ICalculationNode(IDeconstructible):

    def __init__(self, inputs, outputs, shape):
        self._inputs = inputs
        self._outputs = outputs
        self._shape = tuple(shape)
        self._apply = None

    @property
    def apply(self):
        if self._apply is None:
            self._apply = th.function([self.inputs], self.outputs)
        return self._apply

    @property
    def inputs(self):
        return self._inputs

    @property
    def outputs(self):
        return self._outputs

    @property
    def nIn(self):
        return self._shape[0]

    @property
    def nOut(self):
        return self._shape[1]

    @property
    def shape(self):
        return self._shape


class ICalculationNodeWithParams(ICalculationNode):

    def __init__(self, inputs, outputs, shape, params):
        super(ICalculationNodeWithParams, self).__init__(inputs, outputs, shape)
        self._params = tuple(params)

    def resampleParameters(self, rng):
        return self.copy()

    @property
    def params(self):
        return self._params


class ICalculationPath(ICalculationNode):

    def __init__(self, path):
        shape = (path[0].nIn, path[-1].nOut)
        inputs, outputs = path[0].inputs, path[-1].outputs
        super(ICalculationPath, self).__init__(inputs, outputs, shape)
        self._path = path

    @property
    def path(self):
        return self._path


class ICalculationPathWithParams(ICalculationNodeWithParams):

    def __init__(self, path):
        shape = (path[0].nIn, path[-1].nOut)
        inputs, outputs = path[0].inputs, path[-1].outputs
        params = np.ravel([
            n.params for n in path
            if isinstance(n, ICalculationNodeWithParams)
        ])
        super(ICalculationPathWithParams, self).__init__(
            inputs, outputs, shape, params
        )
        self._path = path

    @property
    def path(self):
        return self._path


class ITrainTestCalculationPath(IDeconstructible):

    def __init__(self, trainPath, testPath):
        self._trainPath = ICalculationPathWithParams(trainPath)
        self._testPath = ICalculationPath(testPath)
        assert self._trainPath.shape == self._testPath.shape

    @property
    def trainPath(self):
        return self._trainPath

    @property
    def testPath(self):
        return self._testPath

    @property
    def shape(self):
        return self._trainPath.shape

    @property
    def nIn(self):
        return self._trainPath.nIn

    @property
    def nOut(self):
        return self._trainPath.nOut
