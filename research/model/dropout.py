# encoding: utf-8
from __future__ import absolute_import, print_function, division

from ..utils.deconstruct import IDeconstructible

__all__ = ['dropout']


class TDropout(IDeconstructible):
    _deconstruct_into_ = ('retainProbability',)

    def __init__(self, retainProbability):
        self.retainProbability = retainProbability


def dropout(retainProbability):
    return TDropout(retainProbability)
