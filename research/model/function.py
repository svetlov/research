# encoding: utf-8

from __future__ import absolute_import, print_function, division

from ..utils.misc import asInputs
from .base import ICalculationNode

__all__ = ['TFunctionLayer']


class TFunctionLayer(ICalculationNode):
    _deconstruct_into_ = ('function', 'shape', 'inputs')

    def __init__(self, function, shape, inputs = None):
        inputs = asInputs(inputs)
        self.function = function
        outputs = function(inputs)
        super(TFunctionLayer, self).__init__(inputs, outputs, shape)
