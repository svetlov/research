# encoding: utf-8

from __future__ import absolute_import, print_function, division

from .base import ICalculationNode

__all__ = ['TInputLayer']


class TInputLayer(ICalculationNode):
    _deconstruct_into_ = ('inputs', 'shape')

    def __init__(self, inputs, shape):
        """Strange node, cause it's may have only one inputs."""
        if isinstance(shape, int):
            shape = (shape, shape)
        elif isinstance(shape, (tuple, list)) and len(shape) == 2:
            shape = shape
        super(TInputLayer, self).__init__(inputs, inputs, shape)
