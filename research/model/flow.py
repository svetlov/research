# encoding: utf-8

from __future__ import absolute_import, print_function, division

from ..transformations.addinput import TAddInputLayer
from ..transformations.connect import TConnectLayers
from ..transformations.function import TExpandFunction
from ..transformations.propagateSize import TPropagateSize
from ..transformations.resampleParameters import TResampleParameters
from ..transformations.dropout import TApplyDropout

from .base import ITrainTestCalculationPath

__all__ = ['TFeedForwardNN']


class TFeedForwardNN(ITrainTestCalculationPath):
    _deconstruct_into_ = ('layers', 'inputs')

    def __init__(self, layers, inputs=None, rng=None, srng=None):
        trainPath, testPath = self.transform(layers, inputs, rng, srng)
        super(TFeedForwardNN, self).__init__(trainPath, testPath)

    def transform(self, layers, inputs, rng, srng):
        simplePathTransformations = [
            TAddInputLayer(inputs),
            TExpandFunction,
            TPropagateSize,
            # TResampleParameters(rng)
        ]
        trainTestPathTransformations = [
            TApplyDropout(srng),
            TConnectLayers
        ]
        for transform in simplePathTransformations:
            layers = transform.apply(layers)

        trainPath, testPath = layers, layers
        for transform in trainTestPathTransformations:
            trainPath, testPath = transform.apply(trainPath, testPath)

        return (trainPath, testPath)
