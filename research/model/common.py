# encoding: utf-8

from __future__ import absolute_import, print_function, division

import numpy as np
import theano as th
import theano.tensor as T

from ..utils.misc import asInputs, initRandomState
from .base import ICalculationNodeWithParams

__all__ = ['TFullyConnectedLayer']


class TFullyConnectedLayer(ICalculationNodeWithParams):
    _deconstruct_into_ = ('shape', 'activation', 'W', 'b', 'params', 'useBias', 'inputs')

    def __init__(
            self, shape, activation,
            W=None, b=None, useBias=True,
            inputs=None, params=None, rng=None
    ):
        self.activation = activation
        self.useBias = useBias
        inputs = asInputs(inputs)
        initializedParams = self._initParameters(shape, W, b, rng)
        params = initializedParams if params is None else params
        outputs = self._getFormula(inputs)
        super(TFullyConnectedLayer, self).__init__(
            inputs, outputs, shape, params
        )

    def resampleParameters(self, rng):
        return self.copy(W=None, b=None, rng=rng)

    def _initParameters(self, shape, W, b, rng=None):
        rng = initRandomState(rng)
        self.W = self._initWeights(rng, shape) if W is None else W
        params = [self.W]
        if self.useBias:
            self.b = self._initBias(rng, shape) if b is None else b
            params.append(self.b)
        else:
            self.b = None
        return params

    def _initWeights(self, rng, shape):
        inDim, outDim = shape
        wValues = np.asarray(
            rng.uniform(
                low=-np.sqrt(6.0 / (inDim + outDim)),
                high=np.sqrt(6.0 / (inDim + outDim)),
                size=(inDim, outDim)
            ), dtype=th.config.floatX
        )
        if self.activation is T.nnet.sigmoid:
            wValues *= 4
        W = th.shared(value=wValues, name='W', borrow=True)
        return W

    def _initBias(self, rng, shape):
        bValues = np.zeros((shape[1],), dtype=th.config.floatX)
        b = th.shared(value=bValues, name='b', borrow=True)
        return b

    def _getFormula(self, inputs):
        activation = self.activation
        linOutput = T.dot(inputs, self.W)
        if self.useBias:
            linOutput += self.b
        outputs = linOutput if activation is None else activation(linOutput)
        return outputs
