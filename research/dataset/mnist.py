# encoding: utf-8

import os
import gzip
import cPickle

import numpy
import theano

from .common import DataSet, loadData as _loadData


DATASET_NAME = 'mnist.pkl.gz'
DATASET_URL = 'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'

loadData = lambda: _loadData(DATASET_NAME, DATASET_URL)
