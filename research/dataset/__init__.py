# encoding: utf-8

from .common import *
from . import iris
from . import letters
from . import mnist
from . import segmentation
from . import vowels
