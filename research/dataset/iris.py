# encoding: utf-8

from .common import DataSet, loadData as _loadData


DATASET_NAME = 'iris.pkl.gz'
DATASET_URL = ''

loadData = lambda: _loadData(DATASET_NAME, DATASET_URL)
