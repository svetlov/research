# encoding: utf-8

import os
import math
import gzip
import cPickle
from collections import namedtuple

import numpy as np
import pandas as pd
import theano as th
import theano.tensor as T

from ..utils.misc import toRoundInt, initRandomStream

__all__ = [
    'DataSet',
    'IDataProvider',
    'TSimpleDataProvider',
    'TRandomSamplesDataProvider',
    'TRandomBatchesDataProvider',
    'TRegressionData',
    'TLabeledData',
    'TBinarizedLabeledData',
    'createData',
    'loadData'
]


DataSet = namedtuple('DataSet', ['trainSet', 'validSet', 'testSet'])


def _binarize(targets):
    # TODO (splinter 2015/12/06) check how could improve performance throw
    # vectorized assign to binary
    assert len(targets.shape) == 1

    nRows = targets.shape[0]
    nColumns = toRoundInt(max(targets)) + 1  # cause of 0 neuron

    binary = np.zeros([nRows, nColumns], dtype=th.config.floatX)
    for iRow, target in enumerate(targets):
        print(iRow, toRoundInt(target))
        binary[iRow, toRoundInt(target)] = 1.0
    return binary


def _getFeaturesVariable(data):
    return data.tFeatures


def _getPlainTargetsVariable(data):
    return data.tTargets


def _getIntTargetsVariable(data):
    return T.cast(data.tTargets, 'int32')


def _getBinaryTargetsVariable(data):
    assert isinstance(data, TBinarizedLabeledData)
    return data.tBinaryTargets


class IDataProvider(object):
    TARGETS_TYPE = {
        'float': _getPlainTargetsVariable,
        'int': _getIntTargetsVariable,
        'binary': _getBinaryTargetsVariable,
        'features': _getFeaturesVariable
    }
    DEFAULT_TARGETS_TYPE = 'int'

    def __init__(self, data, batchSize=None, targetsType=None):
        self._data = data
        self._features = data.tFeatures

        targetsType = self.DEFAULT_TARGETS_TYPE \
            if targetsType is None \
            else targetsType
        self._targets = self.TARGETS_TYPE[targetsType](self._data)
        self.expectedTargetsVariable = self._targets.type(name='targets')

        self.batchSize = self.data.size if batchSize is None else batchSize
        self.nBatches = int(math.ceil(self.data.size / self.batchSize))

        self.updates = []

    @property
    def data(self):
        return self._data

    def getBatch(self, iBatch):
        raise NotImplementedError


class TSimpleDataProvider(IDataProvider):
    def __init__(self, data, batchSize=None, targetsType=None):
        super(TSimpleDataProvider, self).__init__(data, batchSize, targetsType)
        self.currentBatchNo = th.shared(0, name='batchIndex')
        currentBatchNoUpdate = T.switch(
            self.currentBatchNo > self.nBatches, 0, self.currentBatchNo + 1
        )
        self.updates.append([self.currentBatchNo, currentBatchNoUpdate])

    def getBatch(self):
        low = self.currentBatchNo * self.batchSize
        high = low + self.batchSize
        return [
            self._features[low:high, ],
            self._targets[low:high]
        ]


class TRandomSamplesDataProvider(IDataProvider):
    def __init__(self, data, batchSize=None, targetsType=None, trng=None):
        super(TRandomSamplesDataProvider, self).__init__(data, batchSize, targetsType)
        self._trng = initRandomStream(trng)
        self.randomIndicies = self._trng.choice(
            a=self.size, size=(self.batchSize,), replace=False
        )

    def getBatch(self):
        return [
            self._features[self.randomIndicies],
            self._targets[self.randomIndicies]
        ]


class TRandomBatchesDataProvider(IDataProvider):
    def __init__(self, data, batchSize=None, targetsType=None, trng=None):
        super(TRandomBatchesDataProvider, self).__init__(data, batchSize, targetsType)
        self._trng = initRandomStream(trng)
        # -1 cause args is size, low, high with inclusive low and high
        self.randomBatchIndex = self._trng.random_integers(
            size=(1, ), low=0, high=(self.nBatches - 1)
        )[0]

    def getBatch(self):
        low = self.randomBatchIndex * self.batchSize
        high = low + self.batchSize
        return [
            self._features[low:high, ],
            self._targets[low:high]
        ]


class TRegressionData(object):
    def __init__(self, features, targets):
        assert features.shape[0] == targets.shape[0]
        self.features = features
        self.targets = targets

        self.tFeatures = th.shared(self.features, name='X', borrow=True)
        self.tTargets = th.shared(self.targets, name='Y', borrow=True)

    @property
    def nFeatures(self):
        return self.features.shape[1]

    @property
    def nTargets(self):
        return self.targets.shape[1]

    @property
    def size(self):
        return self.features.shape[0]


class ILabeledData(object):
    # original features (numpy array)
    features = None
    # original targets labels (could be int/string)
    labels = None
    # targets in mean of expected active Neuron
    targets = None
    # mapping from original target to real target (neuron idx)
    labelToTarget = None

    @property
    def nFeatures(self):
        return self.features.shape[1]

    @property
    def nLabels(self):
        return len(self.labelToTarget.keys())

    @property
    def nTargets(self):
        return len(set(self.labelToTarget.values()))

    @property
    def size(self):
        return self.features.shape[0]


class TLabeledData(ILabeledData):
    def __init__(self, features, labels, labelToTarget=None):
        # check same rows number
        assert labels.shape[0] == features.shape[0]
        # check labels is some class object and contain only one column
        assert len(labels.shape) == 1

        self.features = features.astype(th.config.floatX)
        self.labels = labels
        self.targets = np.zeros(labels.shape, dtype=th.config.floatX)

        uniqueLabels = set(pd.Series(labels).unique())
        if labelToTarget is None:
            # map each class in distinct neuron
            self._labelToTarget = {
                label: neuronIdx for neuronIdx, label in
                enumerate(sorted(uniqueLabels))
            }
        else:
            # check keys in data match keys in map
            assert uniqueLabels == set(labelToTarget.keys())
            # NOTE: (splinter 2015/12/06) it can be passive neurons,
            # which don't have samples
            self._labelToTarget = labelToTarget

        for index, label in enumerate(labels):
            self.targets[index] = self._labelToTarget[label]

        self.tFeatures = th.shared(self.features, name='X', borrow=True)
        self.tTargets = th.shared(self.targets, name='Y', borrow=True)

    @property
    def labelToTarget(self):
        return self._labelToTarget

    @labelToTarget.setter
    def labelToTarget(self, newLabelToTarget):
        for label, newTarget in newLabelToTarget.items():
            labelRows = np.where(self.labels == label)[0]
            self.targets[labelRows] = newTarget
            self._labelToTarget[label] = newTarget


class TBinarizedLabeledData(TLabeledData):
    def __init__(self, features, labels, labelToTarget=None):
        super(TBinarizedLabeledData, self).__init__(features, labels, labelToTarget)
        self.binaryTargets = _binarize(self.targets)
        self.tBinaryTargets = th.shared(self.binaryTargets, name='Y', borrow=True)

    @TLabeledData.labelToTarget.setter
    def labelToTarget(self, newLabelToTarget):
        super(TBinarizedLabeledData, self).labelToTarget = newLabelToTarget
        binaryRow = np.zeros([self.nTargets], dtype=th.config.floatX)
        for label, newTarget in newLabelToTarget.iteritems():
            labelRows = np.where(self.labels == label)[0]
            # edit binary row -> set newTarget element to 1.0
            binaryRow[newTarget] = 1.0
            self.binaryTargets[labelRows] = binaryRow
            # undo edit of binary row
            binaryRow[newTarget] = 0.0

    @staticmethod
    def createFromLabeledData(data):
        return TBinarizedLabeledData(data.features, data.labels, data.labelToTarget)


def createData(name, trn, vld, txt):
    # TODO (splinter 2015/12/26) implement it for Dolenko
    raise NotImplementedError


def loadData(name, url=None):
    ''' Loads the dataset

    :type dataset: string
    :param dataset: the path to the dataset
    '''

    path = os.path.join(os.path.split(__file__)[0], '..', '..', 'data', name)
    if not os.path.isfile(path):
        if url is None:
            raise RuntimeError("unknown dataset, try to download it first")
        # try to download it with given url
        import urllib
        urllib.urlretrieve(url, path)

    # Load the dataset
    with gzip.open(path, 'rb') as bfh:
        [trainX, trainY], [validX, validY], [testX, testY] = cPickle.load(bfh)

    trainX = np.array(trainX, dtype=th.config.floatX)
    trainY = np.array(trainY)
    validX = np.array(validX, dtype=th.config.floatX)
    validY = np.array(validY)
    testX = np.array(testX, dtype=th.config.floatX)
    testY = np.array(testY)

    data = DataSet(
        TLabeledData(trainX, trainY),
        TLabeledData(validX, validY),
        TLabeledData(testX, testY)
    )
    return data
