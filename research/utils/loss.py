# encoding: utf-8

import theano
import theano.tensor as T

__all__ = [
    'makeLossEstimator',
    'nll', 'mse',
    'classifyErrors',
    'L1', 'L2'
]


def makeLossEstimator(model, data, loss, updates=None):
    givenModelInputs, givenModelDesiredTargets = data.getBatch()

    givens = {
        model.inputs: givenModelInputs,
        data.expectedTargetsVariable: givenModelDesiredTargets
    }

    if callable(loss):
        loss = loss(model.outputs, data.expectedTargetsVariable)

    fn = theano.function(
        inputs=[],
        outputs=loss,
        updates=updates,
        givens=givens,
    )
    return fn


def nll(realVariable, targetsVariable):
    "Evaluate negative log likelihood of model with respect to desired output."
    logProb = T.log(realVariable)
    rowIndicies = T.arange(targetsVariable.shape[0])
    columnIndicies = targetsVariable
    return - T.mean(logProb[rowIndicies, columnIndicies])


def mse(realVariable, targetsVariable):
    return T.mean((realVariable - targetsVariable) ** 2)


def classifyErrors(realVariable, targetsVariable):
    yPred = T.argmax(realVariable, axis=1)
    return T.mean(T.neq(yPred, targetsVariable))


def L1(params):
    assert params
    return T.sum([T.sum(abs(param)) for param in params])


def L2(params):
    assert params
    return T.sum([T.sum(param ** 2) for param in params])
