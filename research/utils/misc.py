# encoding: utf-8

from __future__ import absolute_import, print_function, division

import numpy as np
import theano as th
import theano.tensor as T

if th.config.device.startswith('gpu'):
    from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
else:
    from theano.tensor.shared_randomstreams import RandomStreams

RANDOM_SEED = 12345

__all__ = [
    'asInputs',
    'initRandomState',
    'initRandomStream',
    'toRoundInt'
]


def initRandomState(rng):
    if rng is None:
        rng = np.random.RandomState(RANDOM_SEED)
    elif isinstance(rng, int):
        rng = np.random.RandomState(rng)
    elif rng == 'random':
        from random import randint
        return np.random.RandomState(randint(0, 9999999))
    return rng


def initRandomStream(srng):
    if srng is None:
        srng = RandomStreams(RANDOM_SEED)
    elif isinstance(srng, int):
        srng = RandomStreams(srng)
    elif srng == 'random':
        from random import randint
        srng = RandomStreams(randint(0, 9999999))
    return srng


def toRoundInt(value):
    return int(round(value))


def asInputs(inputs):
    if inputs is None:
        inputs = T.matrix('inputs')
    elif isinstance(inputs, T.TensorVariable):
        pass
    else:
        raise RuntimeError('Cannot deduce inputs')
    return inputs
