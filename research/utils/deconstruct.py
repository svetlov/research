#Embedded file name: /Users/splinter/projects/science/tools/research/research/utils/deconstruct.py
from __future__ import absolute_import, print_function, division
__all__ = ['IDeconstructible']

class IDeconstructible(object):
    _deconstruct_into_ = tuple()

    def deconstruct(self):
        members = {}
        for member in self._deconstruct_into_:
            members[member] = getattr(self, member)

        return members

    def copy(self, **kwargs):
        members = self.deconstruct()
        members.update(kwargs)
        return self.__class__(**members)
