#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function
from __future__ import division

import time
from itertools import groupby
from operator import itemgetter

import theano.tensor as T

from research.dataset import vowels, SupervisedMiniBatches
from research.model.common import RegressionModel
from research.trainer import MomentumTrainer, EarlyStoppingGradientTrainer
from research.utils.loss import nll, classifyErrors, makeLossEstimator


if __name__ == '__main__':
    trn, vld, tst = vowels.loadData()

    model = RegressionModel(trn.nFeatures, trn.nTargets, T.nnet.softmax)
    trainData = SupervisedMiniBatches(trn.tFeatures, trn.tIntTargets, 200)
    trainer = MomentumTrainer(model, trainData, nll, 0.01, 0.0)

    validData = SupervisedMiniBatches(vld.tFeatures, vld.tIntTargets)
    validate = makeLossEstimator(model.tst, validData, classifyErrors)

    testData = SupervisedMiniBatches(tst.tFeatures, tst.tIntTargets)
    test = makeLossEstimator(model.tst, testData, classifyErrors)

    estrainer = EarlyStoppingGradientTrainer(trainer, validate)
    epochs = estrainer.train(nEpochs=20000)

    start = time.clock()
    for epoch, records in groupby(epochs, itemgetter(0)):
        for _1, _1, vldloss in records:
            pass
        print("at epoch {} error on validation set is {:.2f}".format(epoch, vldloss * 100))
    end = time.clock()

    print("Error on test set after training is {:.4f}".format(test() * 100))
    print("{} epochs in {} seconds, average: {} epoch/sec".format(
        epoch, end - start, epoch/(end - start)
    ))
