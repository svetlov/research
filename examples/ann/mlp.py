#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function
from __future__ import division

import time
from itertools import groupby
from operator import itemgetter

import theano

from research.dataset import mnist, TSimpleDataProvider, TRandomBatchesDataProvider
from research.model.flow import TFeedForwardNN
from research.model.dropout import dropout

from research.trainer import TMomentumTrainer, TEarlyStoppingGradientTrainer
from research.utils.loss import nll, L2, classifyErrors, makeLossEstimator


if __name__ == '__main__':
    trn, vld, tst = mnist.loadData()

    batchSize = 50
    layers = [trn.nFeatures, 2048, dropout(0.5), 2048, dropout(0.5), trn.nTargets]
    # layers = [trn.nFeatures, 150, 80, trn.nTargets]
    model = TFeedForwardNN(layers)
    weights = [p for p in model.trainPath.params if p.name == 'W']

    regularization = 0.00001 * L2(weights)
    cost = lambda outputs, targets: nll(outputs, targets) + regularization

    alpha, momentum = theano.shared(0.001, name='alpha'), 0.9
    trnData = TRandomBatchesDataProvider(trn, batchSize)
    trainer = TMomentumTrainer(model, trnData, cost, alpha, momentum)

    validationData = TSimpleDataProvider(vld)
    validate = makeLossEstimator(model.testPath, validationData, classifyErrors)

    testData = TSimpleDataProvider(tst)
    test = makeLossEstimator(model.testPath, testData, classifyErrors)

    estrainer = TEarlyStoppingGradientTrainer(trainer, validate)

    start = time.clock()

    for epoch, records in groupby(estrainer.train(200000), itemgetter(0)):
        for record in records:
            _1, _2, vldScore = record
        print("at {} error on validation dataset is {:.3f}".format(
            epoch, 100 * vldScore
        ))
        # if epoch % 150 == 0:
        #     alpha.set_value(alpha.get_value() / 2)
        #     print("new alpha = {}".format(alpha.get_value()))

    end = time.clock()

    print("after training validation error is {}% and test error is {}%".format(
        validate() * 100, test() * 100
    ))
    print("{} epochs in {} seconds, average: {} epoch/sec".format(
        epoch, end - start, epoch/(end - start)
    ))
