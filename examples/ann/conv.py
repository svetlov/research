#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function
from __future__ import division

import time

from research.dataset import mnist
from research.ann.mlp import TanhLayerFactory, SoftmaxLayerFactory, MLP
from research.ann.convolution import (
    ConvolutionPoolLayerFactory,
    BatchedReshape3DLayerFactory,
    FromConvPoolToFullyLayerFactory
)
from research.trainer import GradientTrainer
from research.utils.loss import nll, L1, L2, classifyErrors

if __name__ == '__main__':
    nkernels = [20, 50]
    batchSize = 250
    layers = [
        BatchedReshape3DLayerFactory([batchSize, 1, 28, 28]),
        ConvolutionPoolLayerFactory(
            imageShape=[batchSize, 1, 28, 28],
            filterShape=[nkernels[0], 1, 5, 5],
            pool=(2, 2)
        ),
        ConvolutionPoolLayerFactory(
            imageShape=[batchSize, nkernels[0], 12, 12],
            filterShape=[nkernels[1], nkernels[0], 5, 5],
            pool=(2, 2)
        ),
        FromConvPoolToFullyLayerFactory(),
        TanhLayerFactory(500),
        SoftmaxLayerFactory(10)
    ]
    for l in layers:
        print(l.size)
    model = MLP(28*28, layers)

    data = mnist.loadData()
    trainer = GradientTrainer(model, nll, data.trainSet, batchSize, 0.01)
    errors = trainer.earlyStoppingTrainGenerator(classifyErrors, data.validationSet)
    msg = "at epoch {} error on train set is {:.6f}, on validation set is {:.3f}%"
    start = time.clock()
    for epoch, (trn, vld) in enumerate(errors):
        print(msg.format(epoch, trn, vld * 100))
    end = time.clock()

    print("{} epochs in {} seconds, average: {} epoch/sec".format(
        epoch, end - start, epoch/(end - start)
    ))
