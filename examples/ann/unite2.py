#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function
from __future__ import division

import sys
import StringIO
import time
from random import randint
from collections import defaultdict
from itertools import product

import numpy as np
import pandas as pd

from research.dataset import vowels, TRandomBatchesDataProvider, TSimpleDataProvider

from research.model import TFeedForwardNN

from research.trainer.unite import UniteNodeBuilder
from research.trainer import TMomentumTrainer, TEarlyStoppingGradientTrainer
from research.utils.loss import mse, nll, classifyErrors, makeLossEstimator

N_HIDDEN = 3
BATCH_SIZE = 5
LEARNING_RATE = 0.03


def buildModelAndTrainer(trnData, vldData):
    nHidden, batchSize, learningRate, momentum = N_HIDDEN, BATCH_SIZE, LEARNING_RATE, 0.0
    model = TFeedForwardNN([trnData.nFeatures, nHidden, trnData.nTargets], rng=randint(0, 9999))

    trnData = TRandomBatchesDataProvider(trnData, batchSize, trng=randint(0, 9999))
    trainer = TMomentumTrainer(model, trnData, nll, learningRate, momentum)

    vldData = TSimpleDataProvider(vldData)
    validate = makeLossEstimator(model.testPath, vldData, classifyErrors)
    validate()

    estrainer = TEarlyStoppingGradientTrainer(trainer, validate)
    return model, estrainer


def winnerTakesAll(outputs):
    return outputs.argmax(axis=1)


def winnerGreaterThanThreshold(outputs):
    threshold = 0.2
    noWinnerLabel = -1

    positions = outputs.argmax(axis=1)
    values = outputs[np.arange(outputs.shape[0]), positions]
    positions[np.where(values < threshold)] = noWinnerLabel
    return positions


if __name__ == '__main__':
    # nHidden / batchSize / lr / begin/ timeout
    trnStats = defaultdict( # nHidden
        lambda: defaultdict( # batchSize
            lambda: defaultdict( # lr
                lambda: defaultdict(dict)  # begin / timeout
            )
        )
    )


    vldStats = defaultdict( # nHidden
        lambda: defaultdict( # batchSize
            lambda: defaultdict( # lr
                lambda: defaultdict(dict)  # begin / timeout
            )
        )
    )

    presets = product(
        [4,],
        (1, 5),
        (0.1, 0.03, 0.005),
        (1, 5, 20),
        (1, 3, 5)
    )
    for nHidden, batchSize, lr, begin, timeout in presets:
        global N_HIDDEN
        N_HIDDEN = nHidden
        global BATCH_SIZE
        BATCH_SIZE = batchSize
        global LEARNING_RATE
        LEARNING_RATE = lr

        outputfilename = "vowels_{}_{}_{}_{}_{}".format(N_HIDDEN, BATCH_SIZE, LEARNING_RATE, begin, timeout)
        sys.stdout = open(outputfilename, 'w')

        print("nHidden={}\nbatchSize={}\nlr={}\nstarting_epoch={}\ntimeout={}\n\n".format(
            N_HIDDEN, BATCH_SIZE, LEARNING_RATE, begin, timeout
        ))

        realstdout = sys.stdout
        sys.stdout = StringIO.StringIO()

        trnErrors = []
        vldErrors = []
        for i in xrange(100):
            print("begin training model {}".format(i))
            trn, vld, tst = vowels.loadData()

            builder = UniteNodeBuilder(
                trn, vld,
                buildModelAndTrainer,
                winnerGreaterThanThreshold
            )

            start = time.clock()
            model = builder.build(begin, timeout)
            end = time.clock()

            trnData = TSimpleDataProvider(trn)
            validateOnTrn = makeLossEstimator(model.testPath, trnData, classifyErrors)
            vldData = TSimpleDataProvider(vld)
            validateOnVld = makeLossEstimator(model.testPath, vldData, classifyErrors)

            trnErrors.append(validateOnTrn())
            vldErrors.append(validateOnVld())
            print("{}-th model ended. trnError={}\t vldError={}".format(i, trnErrors[-1], vldErrors[-1]))

        out = sys.stdout.getvalue()
        sys.stdout = realstdout

        trnSeries = pd.Series(np.array(trnErrors))
        print("Trn series describe:\n{}\n\n".format(trnSeries.describe()))

        vldSeries = pd.Series(np.array(vldErrors))
        print("Vld series describe:\n{}\n\n".format(vldSeries.describe()))

        print(out)

        trnStats[nHidden][batchSize][lr][begin][timeout] = [
            trnSeries.mean(), trnSeries.min(), trnSeries.max(),
            trnSeries.std(),
            trnSeries.quantile(0.25), trnSeries.quantile(0.75),
        ]

        vldStats[nHidden][batchSize][lr][begin][timeout] = [
            vldSeries.mean(), vldSeries.min(), vldSeries.max(),
            vldSeries.std(),
            vldSeries.quantile(0.25), vldSeries.quantile(0.75),
        ]

    import dill
    dill.dump(trnStats, open('trnStats.pkl', 'w'))
    dill.dump(vldStats, open('vldStats.pkl', 'w'))

    # mappings = sorted(builder.mappings.keys())
    # for label, target in builder.mappings[mappings[-1]].items():
    #     print("{} -> {}".format(label, target))
