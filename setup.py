# -*- coding: utf-8 -*-

import re
from setuptools import setup, find_packages


if __name__ == '__main__':
    setup(
        name='research',
        version='0.0.1',
        author='Vsevolod Svetlov',
        author_email='svetlov.vsevolod@gmail.com',
        packages=find_packages(),
    )
