# encoding: utf-8

import pytest

import theano.tensor as T

from research.dataset import mnist
from research.trainer import GradientTrainer
from research.utils.loss import negativeLogLikelihood as NLL, classifyErrors


def test_all():
    model = RegressionModel(28*28, 10, T.nnet.softmax)
    data = mnist.loadData()
    trainer = GradientTrainer(model, NLL, data.trainSet, 600)
    trainer.earlyStoppingTrain(classifyErrors, data.validationSet)
